export class Log {
	datetime: string;
	exerciseId: number;
	id: number;
	repetitions: number;
	weight: number;
}
