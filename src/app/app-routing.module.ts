import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LogsComponent } from './logs/logs.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
	{ path: 'logs', component: LogsComponent },
	{ path: 'login', component: LoginComponent },

	{ path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [ RouterModule ]

})
export class AppRoutingModule { }
