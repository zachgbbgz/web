import { Injectable } from '@angular/core';
import { LOGS } from './mock-logs';
import { Log } from './log'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  private logsUrl = 'hello';

  constructor(private http: HttpClient) { }


  getLogs(): Observable<Log[]> {
  	return this.http.get<Log[]>('http://localhost:80/test_logs');
  }

}
