import { Component, OnInit } from '@angular/core';
import { LogService } from "../log.service"
import { Log } from '../log'

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {

  logs: Log[];

  constructor(private logService: LogService) { }

  ngOnInit() {
  	this.getLogs();
  }

  getLogs(): void {
  	this.logService.getLogs().subscribe(logs => this.logs = logs);
  }

}
